# React SSR Starting Point

This project was created because I was starting yet another SSR React app and I
was tired of starting from scratch and hunting down tutorial after tutorial of
setting all my desired settings up again.

This app is a combined SSR React app and API with authentication. Sessions and
users are stored in Mongo.

## Libraries Used

* React (of course)
* Redux
* React Router
* Passport
* Styled JSX
* Mongoose (for server-side models)
* And a whole lot more!

## Setup

1. `git clone` this badboy somewhere you want to start a project
2. `npm install`
3. Setup Mongo and make sure you have authorization required on it (or just change the connection string if you don't want to do that)
4. Copy `env.example` to `.env` and fill out all applicable variables. If you don't want something that's in there, or not sure what something does in there, search the project for the variable and examine / change that code!

## Developing

`npm run dev`

This starts the dev server and opens a browser window to the page served by express (not webpack)

## Deploying

`npm run build` to create a production build, then `npm start` to start that server up. (I'd suggest using something like pm to run that instead though...)

## Future

I know prod build needs some love, and maybe I'll come back to this and update it, maybe not. Next project I start perhaps! :)
