import bcrypt from 'bcryptjs';
import passport from 'passport';

import User from './models/User';

const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});

passport.use(
  new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
    User.findOne({ email: email }).then((user) => {
      bcrypt.compare(password, user.password, (err, isMatch) => {
        if (err) throw err;

        if (isMatch) {
          return done(null, user);
        } else {
          return done(null, false, { message: "Wrong password" });
        }
      });
    })
    .catch(err => {
      return done(null, false, { message: err });
    });
  })
);

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.FACEBOOK_APP_ID,
      clientSecret: process.env.FACEBOOK_APP_SECRET,
      callbackURL: process.env.FACEBOOK_CALLBACK_URL,
      profileFields: [
        'id',
        'picture.width(200).height(200)',
        'first_name',
        'middle_name',
        'last_name',
        'email'
      ]
    },
    (accessToken, refreshToken, profile, done) => {
      process.nextTick(async () => {
        const facebookUser = {
          email: profile.emails[0].value,
          imageUrl: profile.photos[0].value,
          userProfileId: profile.id
        };

        User.findOne({ email: email }).then((user) => {
          if (!user) {
            const newUser = new User({ email: facebookUser.email, referred_by: 'Facebook' });

            newUser.save().then(() => {
              done(null, newUser);
            })
          } else {
            done(null, user);
          }
        });
      })
    }
  )
)

module.exports = passport;
