import ejs from 'ejs';
import { flushToHTML } from 'styled-jsx/server';
import fs from 'fs';
import path from 'path';

const file = fs.readFileSync(path.resolve(__dirname, '../index.ejs'), 'ascii');

export default function template(title, initialState = {}, content = "") {
  const bundleRoot = process.env.NODE_ENV === 'production' ? process.env.PROD_BUNDLE_ROOT : process.env.DEV_BUNDLE_ROOT;
  const bundleLocation = `${bundleRoot}/bundle.js`;

  let scripts = `
    <script>
      window.__STATE__ = ${JSON.stringify(initialState)}
    </script>
    <script src="${bundleLocation}"></script>
  `;

  if (process.env.NODE_ENV === 'production') {
    scripts += `
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=${process.env.GA_CODE}"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '${process.env.GA_CODE}');
    </script>`;
  }


  const htmlWebpackPlugin = {
    options: {
      title,
      content,
      scripts,
      styles: flushToHTML()
    }
  };

  const rendered = ejs.render(file, { htmlWebpackPlugin});
  return rendered;
}
