import bcrypt from 'bcryptjs';
import express from 'express';
import passport from 'passport';

import User from '../models/User';

const router = express.Router();

router.post('/logout', (req, res) => {
  req.logout();
  res.status(204).send();
});

router.post('/login', (req, res, next) => {
  passport.authenticate('local', (error, user) => {
    if (error) {
      return res.status(400).json({ error });
    }

    if (!user) {
      return res.status(400).json({ error: 'No user found' });
    }

    req.logIn(user, function(error) {
      if (error) {
        return res.status(400).json({ error });
      }

      const userCopy = { ...user._doc };
      delete userCopy.password;

      return res.status(200).json({ user: userCopy });
    });
  })(req, res, next);
});

router.post('/register', (req, res, next) => {
  passport.authenticate('local', (error) => {
    if (error) {
      return res.status(400).json({ error });
    }

    User.findOne({ email: req.body.email }).then((user) => {
      if (!user) {
        const newUser = new User({ email: req.body.email, password: req.body.password });

        bcrypt.genSalt(10, (error, salt) => {
          if (error) {
            return res.status(500).json({ error });
          }

          bcrypt.hash(newUser.password, salt, (error, hash) => {
            if (error) {
              return res.status(500).json({ error });
            }

            newUser.password = hash;
            newUser.save().then((user) => {
              req.logIn(user, function(error) {
                if (error) {
                  return res.status(500).json({ error });
                }

                return res.status(200).json({ success: `logged in ${user.id}` });
              })
            }).catch((error) => {
              return res.status(500).json({ error });
            })
          })
        });
      } else {
        return res.status(400).json({ error: 'Email already exists' });
      }
    })
  })(req, res, next);
});

router.get('/facebook', passport.authenticate('facebook'));
router.get('/facebook/callback', passport.authenticate('facebook', {
  successRedirect: '/facebook/success',
  failureRedirect: '/'
}))


module.exports = router;
