import express from 'express';
import passport from 'passport';

const router = express.Router();

router.get('/something', (req, res) => {
  if (req.user && req.user.is_admin) {
    return res.json(['some', 'stuff']);
  }

  return res.status(401).json({ message: 'You are not authorized to access this resource.' });
});

module.exports = router;
