import { matchPath } from "react-router-dom";

import routes from '../routes';

export default function fetchData(url, store) {
  const matchingRoutes = routes.filter((route) => matchPath(url, route));
  const pageComponents = matchingRoutes.map((route) => route.component);
  const dataPages = pageComponents.filter((component) => component.serverFetch);
  const dataReqs = dataPages.map((page) => store.dispatch(page.serverFetch()));

  return Promise.all(dataReqs);
}
