import _ from './env';

import bodyParser from 'body-parser';
import connectMongo from 'connect-mongo';
import express from 'express';
import favicon from 'serve-favicon';
import { Helmet } from 'react-helmet';
import mongoose from 'mongoose';
import path from 'path';

import React from 'react';
import { renderToString } from 'react-dom/server';
import session from 'express-session';

import App from '../App';
import createStore from '../redux/store';
import fetchData from './fetchData';
import passport from './passport';
import template from './template';

import adminRoutes from './routes/admin';
import authRoutes from './routes/auth';

const MongoStore = connectMongo(session);
const app = express();
const mongooseConfig = {
  useNewUrlParser: true,
  user: process.env.MONGO_USERNAME,
  pass: process.env.MONGO_PASSWORD,
  authSource: process.env.MONGO_AUTH_SOURCE
};

app.use(favicon(path.resolve(__dirname, '../../public/favicon', 'favicon.ico')));

mongoose.connect(process.env.MONGO_URI, mongooseConfig).then(() => {
  console.log(`MongoDB connected ${process.env.MONGO_URI}`);
}).catch((error) => {
  console.log(`MongoDB connection error: ${error}`);
});

app.use('/dist', express.static(path.join(__dirname, '../../dist')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(
  session({
    secret: process.env.SESSION_SECRET_KEY,
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use('/api/auth', authRoutes);
app.use('/api/admin', adminRoutes);

// Everything else (aka, the react app)
app.get('*', (req, res) => {
  const initialState = {};
  process.env.COOKIE_HEADER = req.headers.cookie;

  if (req.user && req.user.email) {
    const user = { ...req.user._doc };
    delete user.password;
    initialState.session = { user };
  }

  const store = createStore(initialState);
  const promise = fetchData(req.url, store);

  promise.then(() => {
    const initialState = store.getState();
    const context = {};
    const content = renderToString(<App req={req} store={store} context={context} />);
    const helmet = Helmet.renderStatic();

    if (context.url) {
      return res.redirect(302, context.url);
    }

    const response = template(helmet.title.toString(), initialState, content);
    res.send(response);
  });
});

app.listen(process.env.SERVER_PORT, () => console.log(`App server is listening on port ${process.env.SERVER_PORT}!`));
