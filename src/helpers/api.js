import axios from 'axios';

const configuration = {
  baseURL: process.env.API_ROOT
};

const instance = axios.create(configuration);
instance.interceptors.request.use(function(config) {
  if (process.env.COOKIE_HEADER) {
    config.headers = { cookie: process.env.COOKIE_HEADER };
  }
  return config;
});

export const api = instance;

export function handleError(error) {
  if (error.response) {
    return { type: 'response', error: error.response.data };
  } else if (error.request) {
    return { type: 'request', error: error.request };
  } else {
    return { type: 'message', error: error.message };
  }
}
