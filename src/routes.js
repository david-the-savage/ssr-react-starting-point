import About from './pages/About';
import AdminDashboard from './pages/admin';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Unauthorized from './pages/Unauthorized';

export default [
  {
    component: About,
    exact: true,
    path: '/about',
    title: 'About'
  },
  {
    component: AdminDashboard,
    exact: true,
    path: '/admin',
    title: 'Admin Dashboard'
  },
  {
    component: Home,
    exact: true,
    path: '/',
    title: 'Home'
  },
  {
    component: Login,
    exact: true,
    path: '/login',
    title: 'Login'
  },
  {
    component: Register,
    exact: true,
    path: '/register',
    title: 'Register'
  },
  {
    component: Unauthorized,
    exact: true,
    path: '/unauthorized',
    title: 'Unauthorized'
  }
]
