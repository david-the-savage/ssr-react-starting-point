import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';

import LoadingIndicator from '../../components/Loading';

import { fetchAdminData } from '../../redux/actions/admin';

function AdminDashboard(props) {
  if (!props.session.user) {
    return <Redirect to={{ pathname: '/login', search: '?nextUrl=/admin' }} />;
  }

  if (!props.session.user.is_admin) {
    return <Redirect to="/unauthorized" />
  }

  useEffect(() => {
    props.fetchAdminData();
  }, []);

  function renderData() {
    if (props.admin.loading) {
      return <LoadingIndicator />;
    }

    if (props.admin.data) {
      return props.admin.data.map((data) => (
        <div>{data}</div>
      ))
    }

    return null;
  }

  return (
    <section>
      <Helmet>
        <title>Admin Dashboard</title>
      </Helmet>

      <div>I'm the admin dashboard</div>
      {renderData()}
    </section>
  );
}

const mapStateToProps = ({ admin, session }) => ({
  admin,
  session
});
export default withRouter(connect(mapStateToProps, { fetchAdminData })(AdminDashboard));
