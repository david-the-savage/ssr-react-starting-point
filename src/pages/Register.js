import Cookies from 'js-cookie';
import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';

import { sendRegistration } from '../redux/actions/session';

function Register(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function isLoading() {
    return props.session && props.session.loading;
  }

  function registerUser(event) {
    event.preventDefault();
    props.sendRegistration(email, password);
  }

  function renderError() {
    if (props.session && props.session.error) {
      return (
        <div>{props.session.error}</div>
      );
    }
  }

  function onFacebookLogin() {
    const inOneHour = new Date(new Date().getTime() + 60 * 60 * 1000);
    Cookies.set('lastLocation_before_logging', props.location.pathname, { expires: inOneHour });
    window.location.href = `${window.location.origin}/api/auth/facebook`;
  }

  if (props.session && props.session.user) {
    return (<Redirect to={{ pathname: '/' }} />)
  }

  return (
    <div>
      <Helmet>
        <title>Register</title>
      </Helmet>

      <button onClick={onFacebookLogin}>Continue with Facebook</button>

      <form onSubmit={registerUser}>
        {renderError()}

        <fieldset disabled={isLoading()}>
          <div>
            <label>Email:</label>
            <input type="email" value={email} onChange={(event) => setEmail(event.target.value)} />
          </div>

          <div>
            <label>Password:</label>
            <input type="password" value={password} onChange={(event) => setPassword(event.target.value)} />
          </div>

          <div>
            <button type="submit">Submit</button>
          </div>
        </fieldset>
      </form>
    </div>
  );
}

const mapStateToProps = ({ session }) => ({
  session
});
export default withRouter(connect(mapStateToProps, { sendRegistration })(Register));
