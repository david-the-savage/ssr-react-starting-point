import React from 'react';
import { Helmet } from 'react-helmet';

export default function Unauthorized() {
  return (
    <div>
      <Helmet>
        <title>Unauthorized</title>
      </Helmet>

      Sorry, you're not authorized to view that page.
    </div>
  );
}
