import React from 'react';
import { Helmet } from 'react-helmet';

export default function About() {
  return (
    <>
      <Helmet>
        <title>About</title>
      </Helmet>

      <div>I'm the about page</div>

      <style jsx>{`
        div {
          background: red;
        }
      `}</style>
    </>
  );
}
