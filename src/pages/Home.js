import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';

import LoadingIndicator from '../components/Loading';

import { fetchHomeData } from '../redux/actions/home';

function Home({ getData, home }) {
  useEffect(() => {
    if (!home.data) {
      getData();
    }
  }, []);

  if (home.data) {
    return (
      <>
        <Helmet>
          <title>Home</title>
        </Helmet>

        <div>I have data</div>
      </>
    )
  }

  if (home.error) {
    return (
      <div>I have error</div>
    )
  }

  return <LoadingIndicator />
}

Home.serverFetch = fetchHomeData;
const mapStateToProps = ({ home }) => ({ home });
export default connect(mapStateToProps, { getData: fetchHomeData })(Home);
