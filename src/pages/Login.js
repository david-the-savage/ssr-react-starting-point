import Cookies from 'js-cookie';
import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';

import { sendCredentials } from '../redux/actions/session';

function Login(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function isLoading() {
    return props.session && props.session.loading;
  }

  function loginUser(event) {
    event.preventDefault();
    props.sendCredentials(email, password);
  }

  function renderError() {
    if (props.session && props.session.error) {
      return (
        <div>{props.session.error}</div>
      );
    }
  }

  function onFacebookLogin() {
    const inOneHour = new Date(new Date().getTime() + 60 * 60 * 1000);
    Cookies.set('lastLocation_before_logging', props.location.pathname, { expires: inOneHour });
    window.location.href = `${window.location.origin}/api/auth/facebook`;
  }

  if (props.session && props.session.user) {
    let pathname = '/';

    if (props.location.search) {
      pathname = props.location.search.substr(10);
    }

    return (<Redirect to={pathname} />)
  }

  return (
    <div>
      <Helmet>
        <title>Login</title>
      </Helmet>

      <button onClick={onFacebookLogin}>Continue with Facebook</button>

      <form onSubmit={loginUser}>
        {renderError()}

        <fieldset disabled={isLoading()}>
          <div>
            <label>Email:</label>
            <input type="email" value={email} onChange={(event) => setEmail(event.target.value)} />
          </div>

          <div>
            <label>Password:</label>
            <input type="password" value={password} onChange={(event) => setPassword(event.target.value)} />
          </div>

          <div>
            <button type="submit">Submit</button>
          </div>
        </fieldset>
      </form>
    </div>
  );
}

const mapStateToProps = ({ session }) => ({
  session
});
export default withRouter(connect(mapStateToProps, { sendCredentials })(Login));
