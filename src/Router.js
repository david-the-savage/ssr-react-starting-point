import React, { useEffect } from 'react';

import {
  BrowserRouter,
  Route,
  StaticRouter,
  Switch,
  useLocation
} from 'react-router-dom';

import { isServer } from './helpers';
import routes from './routes';
import Navigation from './components/Navigation';

const Router = isServer() ? StaticRouter : BrowserRouter;

function PageChangeUpdates() {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);

    if (process.env.NODE_ENV === 'production') {
      gtag('config', process.env.GA_CODE, { 'page_path': pathname });
    }
  }, [pathname]);

  return null;
}

export default function ApplicationRouter({ context, req }) {
  let location;

  if (req) {
    location = req.url;
  }

  return (
    <Router location={location} context={context}>
      <PageChangeUpdates />
      <Navigation />

      <Switch>
        {routes.map((route) => (
          <Route
            component={route.component}
            exact={route.exact}
            key={route.path}
            path={route.path}
          />
        ))}
      </Switch>
    </Router>
  );
}
