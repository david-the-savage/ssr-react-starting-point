import React from 'react';
import { Provider } from 'react-redux';

import Router from './Router';

export default function App({ context, req, store }) {
  return (
    <Provider store={store}>
      <Router context={context} req={req} />
    </Provider>
  )
}
