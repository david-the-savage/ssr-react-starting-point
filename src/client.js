import React from 'react';
import { hydrate } from 'react-dom';

import App from './App';
import createStore from './redux/store';

const store = createStore(window.__STATE__);

hydrate(
  <App store={store} />,
  document.querySelector('#app')
);
