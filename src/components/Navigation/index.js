import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { logoutUser } from '../../redux/actions/session';

function Navigation(props) {
  function logoutUser() {
    props.logoutUser();
  }

  function renderAdminLink() {
    if (props.session.user.is_admin) {
      return (
        <>
          <li>
            <Link to="/admin">Admin Dashboard</Link>
          </li>
        </>
      )
    }
  }

  function renderSessionLinks() {
    if (!props.session.user) {
      return (
        <>
          <li>
            <Link to="/login">Login</Link>
          </li>
          <li>
            <Link to="/register">Register</Link>
          </li>
        </>
      )
    } else {
      return (
        <>
          {renderAdminLink()}
          <li>
            <Link to="#" onClick={logoutUser}>Logout</Link>
          </li>
        </>
      )
    }
  }

  return (
    <nav>
      <ul className="list">
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
        {renderSessionLinks()}
      </ul>

      <style jsx>{`
        ul {
          background-color: aqua;
        }
      `}</style>
    </nav>
  )
}

const mapStateToProps = ({ session }) => ({
  session
});
export default connect(mapStateToProps, { logoutUser })(Navigation);
