import { api } from '../../helpers';

import {
  FETCHING_ADMIN_DATA,
  FETCHED_ADMIN_DATA,
  ERROR_FETCHING_ADMIN_DATA
} from '../constants/admin';

function fetchingAdminData() {
  return ({ type: FETCHING_ADMIN_DATA });
}

function fetchedAdminData(data) {
  return ({ type: FETCHED_ADMIN_DATA, data });
}

function errorFetchingAdminData(error) {
  return ({ type: ERROR_FETCHING_ADMIN_DATA, error });
}

export function fetchAdminData() {
  return (dispatch) => {
    dispatch(fetchingAdminData());

    return api.get('http://localhost:3000/api/admin/something').then((res) => {
      dispatch(fetchedAdminData(res.data));
    }).catch((error) => {
      dispatch(errorFetchingAdminData(error.response.data));
    });
  }
}
