import { api } from '../../helpers';

import {
  FETCHING_HOME_DATA,
  FETCHED_HOME_DATA,
  ERROR_FETCHING_HOME_DATA
} from '../constants/home';

function fetchingHomeData() {
  return ({ type: FETCHING_HOME_DATA });
}

function fetchedHomeData(data) {
  return ({ type: FETCHED_HOME_DATA, data });
}

function errorFetchingHomeData(error) {
  return ({ type: ERROR_FETCHING_HOME_DATA, error });
}

export function fetchHomeData() {
  return (dispatch) => {
    dispatch(fetchingHomeData());

    return api.get('https://jsonplaceholder.typicode.com/photos').then((res) => {
      dispatch(fetchedHomeData(res.data));
    }).catch((error) => {
      dispatch(errorFetchingHomeData(error.response.data));
    });
  }
}
