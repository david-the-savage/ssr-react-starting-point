import { api } from '../../helpers';

import {
  LOGIN_USER,
  LOGIN_ERROR,
  SENDING_CREDENTIALS,

  REGISTER_USER,
  REGISTER_ERROR,
  SENDING_REGISTRATION,

  LOGOUT_USER,
  LOGOUT_ERROR,
  SENDING_LOGOUT
} from '../constants/session';

function loginUser(user) {
  return { type: LOGIN_USER, user };
}

function loginError(response) {
  return { type: LOGIN_ERROR, response };
}

function sendingCredentials() {
  return { type: SENDING_CREDENTIALS };
}

export function sendCredentials(email, password) {
  return (dispatch) => {
    dispatch(sendingCredentials());

    return api.post('/auth/login', { email, password }).then((user) => {
      dispatch(loginUser(user.data.user));
    }).catch((error) => {
      dispatch(loginError(error.response.data));
    })
  }
}

function registerUser(user) {
  return { type: REGISTER_USER, user };
}

function registerError(response) {
  return { type: REGISTER_ERROR, response };
}

function sendingRegistration() {
  return { type: SENDING_REGISTRATION };
}

export function sendRegistration(email, password) {
  return (dispatch) => {
    dispatch(sendingRegistration());

    return api.post('/auth/register', { email, password }).then((user) => {
      dispatch(registerUser(user.data));
    }).catch((error) => {
      dispatch(registerError(error.response.data));
    })
  }
}

function loggedOut() {
  return { type: LOGOUT_USER };
}

function logoutError(response) {
  return { type: LOGOUT_ERROR, response };
}

function sendingLogout() {
  return { type: SENDING_LOGOUT };
}

export function logoutUser() {
  return function(dispatch) {
    dispatch(sendingLogout());

    api.post('/auth/logout', {}).then((user) => {
      dispatch(loggedOut());
    }).catch((error) => {
      dispatch(logoutError(error.response.data));
    })
  }
}
