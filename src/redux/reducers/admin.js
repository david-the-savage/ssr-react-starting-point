import {
  FETCHING_ADMIN_DATA,
  FETCHED_ADMIN_DATA,
  ERROR_FETCHING_ADMIN_DATA
} from '../constants/admin';

const initialState = {};

export default function adminReducer(state = initialState, action) {
  switch(action.type) {
    case FETCHING_ADMIN_DATA:
      return {
        loading: true
      }

    case FETCHED_ADMIN_DATA:
      return {
        data: action.data
      }

    case ERROR_FETCHING_ADMIN_DATA:
      return {
        error: action.error
      }

    default:
      return state;
  }
}
