import {
  FETCHING_HOME_DATA,
  FETCHED_HOME_DATA,
  ERROR_FETCHING_HOME_DATA
} from '../constants/home';

const initialState = {};

export default function homeReducer(state = initialState, action) {
  switch(action.type) {
    case FETCHING_HOME_DATA:
      return {
        loading: true
      }

    case FETCHED_HOME_DATA:
      return {
        data: action.data
      }

    case ERROR_FETCHING_HOME_DATA:
      return {
        error: action.error
      }

    default:
      return state;
  }
}
