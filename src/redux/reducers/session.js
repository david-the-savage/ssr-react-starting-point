const initialState = {};

import {
  LOGIN_USER,
  LOGIN_ERROR,
  SENDING_CREDENTIALS,

  REGISTER_USER,
  REGISTER_ERROR,
  SENDING_REGISTRATION,

  LOGOUT_USER,
  LOGOUT_ERROR,
  SENDING_LOGOUT
} from '../constants/session';

export default function sessionReducer(state = initialState, action) {
  switch (action.type) {
    case SENDING_CREDENTIALS:
    case SENDING_REGISTRATION:
    case SENDING_LOGOUT:
      return {
        loading: true
      }

    case LOGIN_ERROR:
    case REGISTER_ERROR:
    case LOGOUT_ERROR:
      return {
        error: action.response.error
      }

    case LOGIN_USER:
    case REGISTER_USER:
      return {
        user: action.user
      }

    case LOGOUT_USER:
      return initialState;

    default:
      return state;
  }
}
