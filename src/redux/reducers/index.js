import { combineReducers } from 'redux';

import admin from './admin';
import home from './home';
import session from './session';

export default combineReducers({
  admin,
  home,
  session
});
