import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers';

export default function(preloadedState) {
    let middlewares;

    if (process.env.NODE_ENV !== 'production') {
        middlewares = applyMiddleware(thunkMiddleware, logger);
    } else {
        middlewares = applyMiddleware(thunkMiddleware);
    }

    return createStore(rootReducer, preloadedState, middlewares);
};
